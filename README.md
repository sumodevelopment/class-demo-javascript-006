# class-demo-006 - VueJS - basics

A basic class demo that shows how to setup a Vue project using the CLI.

The example contains 3 components: AppHeader, MovieList and MovieListItem

### src/utils
There is a constant declared to mock the data for the app.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
