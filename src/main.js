import Vue from 'vue'
import App from './App.vue'

// A Global Stylesheet.
import './main.css';
import VueRouter from 'vue-router';
import MovieList from './components/MovieList';
import MovieDetail from './components/MovieDetail';

Vue.use( VueRouter );

const routes = [
  { path: '/', component: MovieList },
  { path: '/:movieId', component: MovieDetail }
];

const router = new VueRouter({ routes });

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
