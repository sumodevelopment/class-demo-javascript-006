// A constant that can serve as Mock data.
export const MOVIES = [
    {
        id: "7085a622-c53e-4105-86e9-ff10bd22d2c5",
        title: "The Shawshank Redemption",
        description: "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
        genre: "Drama",
        cover: "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
        rating: "5.0"
    },
    {
        id: "1385a625-d53e-4199-86e9-aad0bd22d2c5",
        title: "Robin Hood: Men in tights",
        description: "A spoof of Robin Hood in general, and Robin Hood: Prince of Thieves (1991) in particular.",
        genre: "Comedy",
        cover: "https://m.media-amazon.com/images/M/MV5BZGYyNmU2NmEtNGU1ZS00YjFkLWI0MWQtZjU2MmUxZDAyN2UxXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
        rating: "4.2"
    },
    {
        id: "9865a625-d53e-4199-86e9-aad0bd2267d5",
        title: "The Naked Gun",
        description: "Incompetent police Detective Frank Drebin must foil an attempt to assassinate Queen Elizabeth II.",
        genre: "Comedy",
        cover: "https://m.media-amazon.com/images/M/MV5BODk1ZWM4ZjItMjFhZi00MDMxLTgxNmYtODFhNWZlZTkwM2UwXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg",
        rating: "4.2"
    },
]