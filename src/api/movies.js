const MOVIES_URL = 'http://localhost:3000/v1/movies';

export const getAllMovies = () => {
    return fetch( MOVIES_URL )
            .then( response => response.json() )
            .then( response => response.data )
}

export const getMovieById = id => {
    return fetch( `${MOVIES_URL}/${id}` )
            .then( response => response.json() )
            .then( response => response.data )
}